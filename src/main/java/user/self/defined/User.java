package user.self.defined;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.freedom.web.mymvc.common.SystemInterface;
import com.freedom.web.mymvc.utils.ByteBufUtils;
import com.freedom.web.mymvc.utils.LoggerUtils;

import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;

public class User implements SystemInterface {

	// logger
	private static final Logger logger = LogManager.getLogger(User.class);

	public void service(FullHttpRequest request, FullHttpResponse response) {
		LoggerUtils.debug(logger, "user invoked...by [" + request.getMethod() + " " + request.getUri() + " "
				+ request.getProtocolVersion());
		// 用户在这里写自己的一些业务逻辑
		String method = request.getMethod().toString();
		String uri = request.getUri();
		String httpVersion = request.getProtocolVersion().toString();
		HttpHeaders head = request.headers();
		String content = ByteBufUtils.getContent(request.content());
		// 用户在此处理,比如可以把content设置为json格式，然后解析json,获取相应的参数
		// 同时，需要返回的内容，就在response里设置即可,写response的内容是由框架来做
		{

		}

	}

}
