package com.freedom.web.mymvc.route;

import java.lang.reflect.Method;

public class Route {
	public Object object;
	public Method method;

	public Route(Object obj, Method met) {
		this.object = obj;
		this.method = met;
	}

	public Object getObject() {
		return object;
	}

	public Method getMethod() {
		return method;
	}

}
